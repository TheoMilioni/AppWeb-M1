<%--
  Created by IntelliJ IDEA.
  User: theo
  Date: 2019-10-19
  Time: 13:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="fr.univlyon1.m1if.m1if03.classes.Billet" %>
<%@ page import="fr.univlyon1.m1if.m1if03.classes.Commentaire" %>
<jsp:useBean id="groupes" beanName="groupes" scope="application" type="java.util.Map"/>

<html>
<head>
    <% Billet billet = (Billet) request.getAttribute("billet");
        int id = Integer.valueOf((int) request.getAttribute("id"));
        String groupe = (String) request.getAttribute("groupe");
    %>
    <title><%=billet.getTitre()%>
    </title>
</head>
<body>
<% response.setIntHeader("Refresh", 5);%>
<% for (Commentaire c : billet.getCommentaires()) {
    out.println("<font face=\"arial\" size=\"0\"> " + c.getDate() + "</font>");
    out.println("<b>" + c.getAuteur() + "</b> : " + c.getCommentaire());
    out.println("<br>");
}%>
</body>
</html>
