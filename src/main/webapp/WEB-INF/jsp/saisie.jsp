<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saisie d'un billet</title>
</head>
<body>
<%@include file="menu.jsp" %>
    <h1>Saisie d'un billet</h1>
    <form method="post" action="groupes/${groupe}/billets">
        <p>
            Titre :
            <input type="text" name="titre" required>
        </p>
        <p>
            Contenu :
            <textarea name="contenu"></textarea>
        </p>
        <p>
            <input type="submit" value="Envoyer">
        </p>

    </form>
</body>
</html>