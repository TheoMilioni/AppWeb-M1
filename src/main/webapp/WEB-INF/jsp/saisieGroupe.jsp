<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Enregistrez un groupe</title>
</head>
<body>
<%@include file="menu.jsp" %>
    <h1>Créer un groupe</h1>
    <form method="post" action="groupes">
        <p>
            Nom du groupe : <input type="text" name="groupe" placeholder="Nom" required>
        </p>
        <p>
            Description :
            <textarea name="description"></textarea>
        </p>
        <p>
            <input type="submit" value="Envoyer">
        </p>

    </form>
</body>
</html>