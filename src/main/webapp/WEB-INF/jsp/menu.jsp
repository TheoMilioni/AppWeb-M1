<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <c:set var="url">${pageContext.request.requestURL}</c:set>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
</head>
<body>
<h2>Hello <%= request.getAttribute("pseudo")%> !</h2>
<h3>Menu</h3>
    <ul>
        <li><a href="groupes">Gestion des groupes</a></li>
        <li><a href="Deco">Déconnexion</a></li>
    </ul>
<hr>
</body>
</html>
