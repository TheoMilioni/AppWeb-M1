<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<jsp:useBean id="groupes" beanName="groupes" scope="application" type="java.util.Map"/>
<!doctype html>
<html>
<head>
    <title>Gestion Groupes</title>
</head>
<body>
<%@include file="menu.jsp" %>
<%response.setIntHeader("Refresh", 5);%>
Cliquez sur le lien suivant pour créer un groupe : <a href="SaisieGroupe"> Créer un groupe</a>
<ul>
    <c:forEach var="entry" items="${groupes}">
        <li><b>${entry.key}</b>
            <ul type="square">
                <li>Description : ${entry.value.getDescription()}</li>
                <li>Propriétaire : ${entry.value.getProprietaire()}</li>
                <li>Nombre de participants : ${entry.value.getParticipants().size()}</li>
                <li>Nombre de billets : ${entry.value.getGestionBillets().nombreBillets()}</li>
                <a href="groupes/${entry.key}/billets">Rejoindre</a>
            </ul>
        </li>
    </c:forEach>
</ul>
</body>
</html>
