<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>
        ${billet.titre}
    </title>
</head>
<body>
<%@include file="menu.jsp" %>
<p > Ceci est un billet du groupe ${groupe}</p>
<p > Billet crée par  ${billet.auteur}</p>
<h1>${billet.titre}</h1>
<div class="contenu">${billet.contenu}</div>
<h3>Commentaires</h3>
<iframe src ="addCommentaire/${groupe}/${id}"
        name="commentaires" style="border: none;width: 100%; height: 200px;">
</iframe>
<hr>
<form method="post" action="addCommentaire/${groupe}/${id}/" target="commentaires">
    <p>
        Commentaire :
        <input type="text" name="commentaire">
        <input type="submit" value="Envoyer">
    </p>
</form>
</body>
</html>