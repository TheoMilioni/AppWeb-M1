<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<html>
<head>
</head>
<body>
<h4>Voici la liste des billets :</h4>
<c:forEach var="billet" items="${gestionBillet.getBillets()}">
    <ul>
        <li>
            <b>${billet.titre}</b>
                <ul>
                    <li>Auteur: ${billet.auteur} </li>
                    <li>Nombre de commentaires: ${billet.commentsSize()} </li>
                </ul>
                <a href="groupes/${groupe}/billets/${groupes.get(groupe).getGestionBillets().getBillets().indexOf(billet)}">
                    Consulter
                </a>
        </li>
    </ul>
</c:forEach>

</body>
</html>
