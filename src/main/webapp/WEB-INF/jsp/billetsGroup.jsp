<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<html>
<head>
    <title>Liste des billets</title>
</head>
<body>
<%@include file="menu.jsp" %>
<h4>Bienvenue dans le groupe ${groupe} !</h4>
<%response.setIntHeader("Refresh", 5);%>
<c:if test="${gestionBillet.getBillets().isEmpty()}">
    <p>Aucun billet n'est disponible. Veuillez saisir un nouveau billet.</p>
</c:if>
<c:if test="${!gestionBillet.getBillets().isEmpty()}">
    <jsp:include page="menuBillets.jsp"/>
</c:if>

<p><a href="SaisieBillet/${groupe}">Saisir un nouveau billet</a></p>
</body>
</html>
