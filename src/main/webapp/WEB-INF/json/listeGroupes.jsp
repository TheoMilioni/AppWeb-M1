<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="org.json.JSONArray" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%
    response.setStatus(200);
    response.setHeader("Content-Type", "application/json");
    List data = (ArrayList) request.getAttribute("data");
    JSONArray json = new JSONArray(data);
    out.print(json);
    out.flush();
%>