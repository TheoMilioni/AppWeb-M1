package fr.univlyon1.m1if.m1if03.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class DataDTO
 * Gestion des données de l'application
 */
public class DataDTO {
    //Attributs de la classe
    private Map<String, Groupe> groupes;
    private Users users;

    /**
     * Constructeur avec paramètres
     *
     * @param groupes
     * @param users
     */
    public DataDTO(Map<String, Groupe> groupes, Users users) {
        this.groupes = groupes;
        this.users = users;
    }

    /**
     * Constructeur par défaut
     */
    public DataDTO() {
        this.groupes = new HashMap<>();
        this.users = new Users();
    }

    /**
     * Pour ajouter un utilisateur
     *
     * @param pseudo
     */
    public void addUser(String pseudo) {
        this.users.addUser(pseudo);
    }

    /**
     * Accesseur de la liste des groupes
     *
     * @return la liste des groupes
     */
    public List<String> getAllGroupes() {
        List<String> groupes = new ArrayList<>();
        for (String groupe : this.groupes.keySet()) {
            groupes.add(groupe);
        }
        return groupes;
    }

    /**
     * Ajout d'un groupe
     *
     * @param groupe
     * @param pseudo
     */
    public void addGroupe(String groupe, String pseudo) {
        if (!this.groupes.containsKey(groupe)) {
            Groupe g = new Groupe();
            g.setNom(groupe);
            g.setProprietaire(pseudo);
            g.addUser(pseudo);
            this.groupes.put(groupe, g);
        }
    }

    /**
     * Ajout d'un groupe
     *
     * @param groupe
     * @param pseudo
     * @param description
     */
    public void addGroupe(String groupe, String pseudo, String description) {
        if (!this.groupes.containsKey(groupe)) {
            Groupe g = new Groupe();
            g.setNom(groupe);
            g.setProprietaire(pseudo);
            g.setDescription(description);
            g.addUser(pseudo);
            this.groupes.put(groupe, g);
        }
    }

    /**
     * Renvoie la representation d'un groupe correspondant passé en paramètre
     *
     * @param groupeId
     * @return Groupe
     */
    public Groupe getGroupeById(String groupeId) {
        return this.groupes.get(groupeId);
    }

    /**
     * Met à jour les données d'un groupe
     *
     * @param nomGroupe
     * @param groupe
     */
    public void updateGroupe(String nomGroupe, Groupe groupe) {
        if (this.groupes.containsKey(nomGroupe)) {
            this.groupes.replace(nomGroupe, groupe);

        } else {
            this.groupes.put(nomGroupe, groupe);
        }
    }

    /**
     * Pour Supprimer un groupe
     *
     * @param groupe
     * @return le groupe supprimer
     */
    public Groupe deleteGroupe(String groupe) {
        return this.groupes.remove(groupe);
    }

    /**
     * Vérifier si un groupe existe
     *
     * @param groupe
     * @return booléen
     */
    public boolean containGroupe(String groupe) {
        return this.groupes.containsKey(groupe);
    }

    /**
     * Vérifie si un utilisateur est membre d'un groupe
     *
     * @param groupe
     * @param pseudo
     * @return booléen
     */
    public boolean isMember(String groupe, String pseudo) {
        if (this.groupes.containsKey(groupe)) {
            return this.groupes.get(groupe).getParticipants().contains(pseudo);
        }
        return false;
    }

    /**
     * Récuperer l'URI de la liste des billets d'un groupe
     *
     * @param groupe
     * @return URI des billlets d'un groupe
     */
    public List<String> getAllBilletsFromGroupe(String uri, String groupe) {
        List<String> listBillets = new ArrayList<>();
        for (int i = 0; i < this.groupes.get(groupe).getGestionBillets().nombreBillets(); i++) {
            listBillets.add(uri + "/" + groupe + "/" + i);
        }
        return listBillets;
    }

    /**
     * Ajouter un billet dans un groupe
     *
     * @param groupe
     * @param billet
     */
    public void addBillet(String groupe, Billet billet) {
        this.groupes.get(groupe).getGestionBillets().add(billet);
    }

    /**
     * Retrouver un billet
     *
     * @param groupe
     * @param idGroupe
     * @return Renvoie un billet
     */
    public Billet getBilletById(String groupe, int idGroupe) {
        return this.groupes.get(groupe).getGestionBillets().getBillet(idGroupe);
    }

    /**
     * Met à jour un billet dont le le numéro est passé en paramètre
     * Ajoute le billet en fin de liste si le idBillet est supérieur à la liste des billets
     *
     * @param groupe
     * @param idBillet
     * @param billet
     */
    public void updateBillet(String groupe, int idBillet, Billet billet) {
        if (idBillet < getGroupes().get(groupe).getGestionBillets().nombreBillets()) {
            getGroupes().get(groupe).getGestionBillets().getBillets().set(idBillet, billet);
        } else {
            getGroupes().get(groupe).getGestionBillets().add(billet);
        }
    }

    /**
     * Supprimer un billet en donnant le numéro du billet
     *
     * @param groupe
     * @param idBillet
     * @return Billet supprimé
     */
    public Billet deleteBillet(String groupe, int idBillet) {
        return this.groupes.get(groupe).getGestionBillets().getBillets().remove(idBillet);
    }

    /**
     * Recupérer la liste des commentaires d'un billet
     *
     * @param uri
     * @param groupe
     * @param billetId
     * @return liste des commentaires du billet
     */
    public List<String> getAllCommentairesFromBillet(String uri, String groupe, int billetId) {
        List<String> listComments = new ArrayList<>();
        int size = this.groupes.get(groupe).getGestionBillets().getBillet(billetId).commentsSize();
        for (int i = 0; i < size; i++) {
            String lien = uri + "/" + groupe + "/" + billetId + "/billets/" + billetId + "/commentaires/" + i;
            listComments.add(lien);
        }
        return listComments;
    }

    /**
     * Ajout d'un commentaire
     *
     * @param groupe
     * @param billetId
     * @param comment
     */
    public void addCommentaire(String groupe, int billetId, Commentaire comment) {
        this.groupes.get(groupe).getGestionBillets().getBillet(billetId).addCommentaire(comment);
    }

    /**
     * Récuperer un Commentaire
     *
     * @param groupe
     * @param billetId
     * @param idComment
     * @return Commentaire
     */
    public Commentaire getCommentaireById(String groupe, int billetId, int idComment) {
        GestionBillets gb = this.groupes.get(groupe).getGestionBillets();
        return gb.getBillet(billetId).getCommentaires().get(idComment);
    }

    /**
     * Met a jour un commentaire
     * @param groupe
     * @param billetId
     * @param idComment
     * @param c
     */
    public void updateCommentaire(String groupe, int billetId, int idComment, Commentaire c) {
        int size = getGroupes().get(groupe).getGestionBillets().getBillet(billetId).commentsSize();
        if (billetId < size){
            GestionBillets gb = getGroupes().get(groupe).getGestionBillets();
            gb.getBillet(billetId).getCommentaires().set(idComment, c);

        }else {
            addCommentaire(groupe, billetId, c);
        }
    }

    /**
     * Supprimer un commentaire
     * @param groupe
     * @param billetId
     * @param commentId
     * @return
     */
    public Commentaire deleteCommentaire(String groupe, int billetId, int commentId){
        GestionBillets gb = this.groupes.get(groupe).getGestionBillets();
        return gb.getBillet(billetId).getCommentaires().remove(commentId);
    }


    /**
     * Fonction qui renvoie la liste des utilisateurs
     *
     * @return users
     */
    public List<String> getAllUsers() {
        return this.users.getUsers();
    }

    /**
     * Accesseurs de la liste des groupes
     *
     * @return groupes
     */
    public Map<String, Groupe> getGroupes() {
        return groupes;
    }

    /**
     * Mutateur de la liste des groupes
     *
     * @param groupes
     */
    public void setGroupes(Map<String, Groupe> groupes) {
        this.groupes = groupes;
    }

    /**
     * Accesseurs de la liste des utlisateurs
     *
     * @return
     */
    public Users getUsers() {
        return users;
    }

    /**
     * Mutateur de la liste des utilisateurs
     *
     * @param users
     */
    public void setUsers(Users users) {
        this.users = users;
    }
}
