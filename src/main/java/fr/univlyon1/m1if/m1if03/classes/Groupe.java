package fr.univlyon1.m1if.m1if03.classes;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Groupe
 * Gestion des groupes
 */
public class Groupe {
    private String nom;
    private String description;
    private String proprietaire;
    private List<String> participants;
    private GestionBillets gestionBillets;

    /**
     * Constructeur par défaut
     */
    public Groupe() {
        this.participants = new ArrayList<>();
        this.gestionBillets = new GestionBillets();
    }

    /**
     * Constructeur avec paramètre
     * @param nom
     * @param description
     * @param proprietaire
     */
    public Groupe(String nom, String description, String proprietaire) {
        this.nom = nom;
        this.description = description;
        this.proprietaire = proprietaire;
        this.gestionBillets = new GestionBillets();
        this.participants = new ArrayList<>();
    }

    /**
     * Fonction qui permet d'ajouter un utilisateur
     * dans la liste des participants
     * @param participant
     */
    public void addUser(String participant){
        this.participants.add(participant);
    }

    /**
     * Accesseurs du nom du groupe
     * @return Le nom du groupe
     */
    public String getNom() {
        return nom;
    }

    /**
     * Mutateur du nom du groupe
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Accesseur de la description du groupe
     * @return La description du groupe
     */
    public String getDescription() {
        return description;
    }

    /**
     * Mutateur de la description du groupe
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Accesseur du propriétaire du groupe
     * @return le propriétaire du groupe
     */
    public String getProprietaire() {
        return proprietaire;
    }

    /**
     * Mutateur du proprietaire du groupe
     * @param proprietaire
     */
    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }

    /**
     * Accesseur de la liste des participants
     * @return la liste des participants
     */
    public List<String> getParticipants() {
        return participants;
    }

    /**
     * Mutateur de la liste des participants
     * @param participants
     */
    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }

    /**
     * Accesseur du gestionnaire des billets
     * @return GestionBillets
     */
    public GestionBillets getGestionBillets() {
        return gestionBillets;
    }

    /**
     * Mutateur du gestionnaire des billets
     * @param gestionBillets
     */
    public void setGestionBillets(GestionBillets gestionBillets) {
        this.gestionBillets = gestionBillets;
    }
}
