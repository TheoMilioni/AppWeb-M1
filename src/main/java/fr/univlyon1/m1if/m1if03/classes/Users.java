package fr.univlyon1.m1if.m1if03.classes;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Users
 * Gestion de la liste des utilisateurs
 */
public class Users {
    private List<String> users;

    public Users() {
        this.users = new ArrayList<>();
    }

    public Users(List<String> users) {
        this.users = users;
    }

    public void addUser(String pseudo) {
        if (!this.users.contains(pseudo)){
            this.users.add(pseudo);
        }
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }



}
