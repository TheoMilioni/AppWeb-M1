package fr.univlyon1.m1if.m1if03.servlets.router;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import fr.univlyon1.m1if.m1if03.classes.DataDTO;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;


@WebServlet(name = "Users", urlPatterns = "/users/*")
public class Users extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatch(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatch(request, response);
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        String[] splitUri = uri.split("/");
        DataDTO dataDTO = (DataDTO) request.getServletContext().getAttribute("dataDTO");

        switch (splitUri.length) {
            case 3: // /users GET liste users
                response.setHeader("Accept", "application/json");
                request.setAttribute("data", dataDTO.getAllUsers());
                request.getServletContext().getRequestDispatcher("/WEB-INF/json/listeGroupes.jsp").forward(request, response);
                break;

            case 4: //  users/login users/logout
                if (splitUri[3].equals("login")) {
                    request.getServletContext().getRequestDispatcher("/Init").forward(request, response);
//                    String body = getRequestBody(request);
//                    JSONObject json = new JSONObject(body);
//                    String pseudo = json.getString("pseudo");
//                    if (pseudo != null && !pseudo.equals("")) {
//                        try {
//                            Algorithm algorithm = Algorithm.HMAC256("secret");
//
//
//                            String token = JWT.create()
//                                    .withIssuer("mescopains")
//                                    .withSubject(pseudo)
//                                    //Expiration du token au bout de 1h
//                                    .withExpiresAt(Date.from(Instant.now().plusSeconds(3600)))
//                                    .sign(algorithm);
//
//                            Cookie tokenAuth = new Cookie("token", token);
//                            response.addCookie(tokenAuth);
//
//                            if (!dataDTO.getAllUsers().contains(pseudo)) {
//                                dataDTO.addUser(pseudo);
//                            }
//                            String link = request.getRequestURL().toString();
//                            response.setHeader("Accept", "application/json");
//                            response.setStatus(201);
//                            response.setHeader("Location", link + "/" + pseudo);
//                            response.addHeader("Authorization", token);
//                            response.setContentType("application/json");
//                            System.out.println(response.getHeader("Authorization"));
//
//                        } catch (JWTCreationException exception) {
//                            //Invalid Signing configuration / Couldn't convert Claims.
//                        }
//
//
//                    } else {
//                        response.sendError(400, "Pas de parametre acceptable dans la requete");
//                    }

                } else if (splitUri[3].equals("logout")) {
                    System.out.println(request.getHeader("Authorization"));
                    if(setCookieExpired(request, response, "token")){
                        response.setStatus(204);
                    }else {
                        response.setStatus(401);
                    }

                } else {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Mauvaise requete");
                }
                break;
            default:
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Mauvaise requete defauk");
        }

    }

    public static boolean setCookieExpired(HttpServletRequest request, HttpServletResponse response,
                                        String nom) {
        String token = (String) request.getHeader("Authorization");
        System.out.println(token);
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(nom)) {
                    cookie.setMaxAge(0);//pour supprimer le cookie et le token ne sera pas accessible
                    response.addCookie(cookie);
                    return true;
                }
            }

        }
        return false;
    }

    /**
     * Gets the request body from the request.
     *
     * @param request the request
     * @return the request body
     */
    private String getRequestBody(final HttpServletRequest request) {
        final StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = request.getReader()) {
            if (reader == null) {
                //logger.debug("Request body could not be read because it's empty.");
                return null;
            }
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder.toString();
        } catch (final Exception e) {
            //logger.trace("Could not obtain the saml request body from the http request", e);
            return null;
        }
    }
}
