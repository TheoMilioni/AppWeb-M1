package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.DataDTO;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/GroupeController")
public class GroupeController extends HttpServlet {

    private ServletContext sc;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pseudo = (String) request.getAttribute("pseudo");

        String groupe = request.getParameter("groupe");
        String description = request.getParameter("description");

        if (groupe != null && !groupe.equals("")) {
            Map<String, Groupe> groupes = (Map<String, Groupe>) sc.getAttribute("groupes");
            if (!groupes.containsKey(groupe)) {
                Groupe g = new Groupe();
                g.setNom(groupe);
                g.setProprietaire(pseudo);
                g.setDescription(description);
                g.addUser(pseudo);
                groupes.put(groupe, g);
            }
            response.setStatus(201);
        }else
            response.sendError(400,"Pas de paramètres acceptables dans la requête");
        request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/listeGroupes.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getHeader("Accept").startsWith("application/json")) {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            request.getServletContext().getRequestDispatcher("/WEB-INF/json/listeGroupes.jsp").forward(request, response);
        }
        else
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/listeGroupes.jsp").forward(request, response);

    }

    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
        DataDTO dataDTO = (DataDTO) sc.getAttribute("dataDTO");
        sc.setAttribute("groupes", dataDTO.getGroupes());
    }
}
