package fr.univlyon1.m1if.m1if03.servlets.router;

import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "Groupes",  urlPatterns = "/groupes/*")
public class Groupes extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatch(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatch(request, response);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatch(request, response);
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatch(request, response);
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String  uri = request.getRequestURI();
        String[] splitUri = uri.split("/");

        // splitUri = ["","tp2_war","groupes","xyz","billets","123","commentaire","abc"]

        switch(splitUri.length) {
            case 3: // /groupes   GET POST OK
                
                request.getServletContext().getRequestDispatcher("/GroupeController").forward(request, response);
                break;

            case 4: // /groupes/{groupesId}  GET PUT DELETE
                //TODO
                break;

            case 5: // /groupes/{groupeId}/billets  GET POST OK
                if(isGroup(request, splitUri[3]) && splitUri[4].equals("billets")) {
                    request.setAttribute("groupe", splitUri[3]);
                    request.getServletContext().getRequestDispatcher("/ListBilletsController").forward(request, response);
                }else {
                    //renvoyer sur une page d'erreur
                    response.sendError(HttpServletResponse.SC_NOT_FOUND,"Groupe \""+splitUri[3]+"\" inexistant");
                }
                break;

            case 6: // /groupes/{groupeId}/billets/{billetId}  GET OK TODO PUT & DELETE
                if(isGroup(request, splitUri[3]) && splitUri[4].equals("billets")) {
                    request.setAttribute("groupe", splitUri[3]);
                    request.setAttribute("id", splitUri[5]);
//                    request.setAttribute("rest", true);

                    request.getServletContext().getRequestDispatcher("/BilletController").forward(request, response);
                }
                break;

            case 7: // /groupes/{groupeId}/billets/{billetId}/commentaires  TODO GET POST
                break;

            case 8: // /groupes/{groupeId}/billets/{billetId}/commentaires/{commentaireId} TODO GET PUT DELETE
                break;

            default:
                //renvoyer sur une page d'erreur
                response.sendError(HttpServletResponse.SC_BAD_REQUEST,"Mauvaise requete");
        }
    }

    private boolean isGroup(HttpServletRequest request, String s) {
        Map<String, Groupe> groupes = (Map<String, Groupe>) request.getServletContext().getAttribute("groupes");
        return groupes.containsKey(s);

    }
}
