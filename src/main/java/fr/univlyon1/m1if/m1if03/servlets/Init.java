package fr.univlyon1.m1if.m1if03.servlets;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import fr.univlyon1.m1if.m1if03.classes.DataDTO;
import fr.univlyon1.m1if.m1if03.classes.Groupe;
import org.json.JSONObject;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Init", urlPatterns = "/Init")
public class Init extends HttpServlet {
    private ServletContext sc;
    private String pseudo;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        pseudo = request.getParameter("pseudo");

        if (pseudo == null) {
            String body = getRequestBody(request);
            JSONObject json = new JSONObject(body);
            pseudo = json.getString("pseudo");
        }
        if (pseudo != null && !pseudo.equals("")) {
            try {
                Algorithm algorithm = Algorithm.HMAC256("secret");


                String token = JWT.create()
                        .withIssuer("mescopains")
                        .withSubject(pseudo)
                        //Expiration du token au bout de 1h
                        .withExpiresAt(Date.from(Instant.now().plusSeconds(3600)))
                        .sign(algorithm);

                Cookie tokenAuth = new Cookie("token", token);
                //probleme avec l'API le path était /tp2/users et non juste /tp2
                tokenAuth.setPath(request.getContextPath());
                response.addCookie(tokenAuth);

                DataDTO dataDTO = (DataDTO) sc.getAttribute("dataDTO");
                if(!dataDTO.getAllUsers().contains(pseudo)){
                    dataDTO.addUser(pseudo);
                }

                response.sendRedirect(request.getContextPath()+"/groupes");
            } catch (JWTCreationException exception){
                //Invalid Signing configuration / Couldn't convert Claims.
            }


        } else {
            response.sendRedirect(request.getContextPath()+"/index.html");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(request.getContextPath()+"/index.html");
    }

    public void init(ServletConfig config) throws ServletException {
        sc = config.getServletContext();
        DataDTO dataDTO = new DataDTO();
        sc.setAttribute("dataDTO", dataDTO);
    }

    private String getRequestBody(final HttpServletRequest request) {
        final StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = request.getReader()) {
            if (reader == null) {
                //logger.debug("Request body could not be read because it's empty.");
                return null;
            }
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder.toString();
        } catch (final Exception e) {
            //logger.trace("Could not obtain the saml request body from the http request", e);
            return null;
        }
    }


}
