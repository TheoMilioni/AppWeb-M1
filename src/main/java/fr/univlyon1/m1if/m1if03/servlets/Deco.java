package fr.univlyon1.m1if.m1if03.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.io.IOException;

@WebServlet(name = "Deco", urlPatterns = "/Deco")
public class Deco extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setCookieExpired(request, response, "token");
        response.setStatus(204);
        response.sendRedirect("index.html");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setCookieExpired(request, response, "token");
        response.setStatus(204);
        response.sendRedirect("index.html");
    }

    public static void setCookieExpired(HttpServletRequest request, HttpServletResponse response,
                                        String nom) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies)
            if (cookie.getName().equals(nom)) {
                cookie.setMaxAge(0);//pour supprimer le cookie et le token ne sera pas accessible
                response.addCookie(cookie);
            }
    }
}