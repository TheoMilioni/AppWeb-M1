package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "BilletController", urlPatterns = "/BilletController")
public class BilletController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupe = (String) request.getAttribute("groupe");
        Map<String, Groupe> groupes = (Map<String, Groupe>) request.getServletContext().getAttribute("groupes");
        GestionBillets gb = groupes.get(groupe).getGestionBillets();
        int id = Integer.valueOf((String) request.getAttribute("id"));
        //s'assurer que le billet existe
        if(gb.nombreBillets()>id) {
            Billet billet = gb.getBillet(id);
            request.setAttribute("billet", billet);
            request.setAttribute("id", id);
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/billetWindow.jsp").forward(request, response);
        }else {
            //renvoyer sur une page d'erreur
            response.sendError(HttpServletResponse.SC_NOT_FOUND,"Billet inexistant");
        }
    }
}
