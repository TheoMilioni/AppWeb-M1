package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "addCommentaire", urlPatterns = "/addCommentaire/*")
public class AddCommentaire extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String comment = (String) request.getParameter("commentaire");

        String uri = request.getRequestURI();
        String[] splitUri = uri.split("/");
        // splitUri = ["","tp2_war","addCommentaire","groupe","id"]

        int id = Integer.parseInt(splitUri[4]);
        request.setAttribute("id", id);
        String groupe = splitUri[3];
        request.setAttribute("groupe", groupe);

        if (comment != null && groupe != null) {
            Map<String, Groupe> g = (Map<String, Groupe>) getServletContext().getAttribute("groupes");
            GestionBillets gb = g.get(groupe).getGestionBillets();
            Billet billet = gb.getBillet(id);
            String auteur = (String) request.getAttribute("pseudo");
            billet.addCommentaire(auteur, comment);
            request.setAttribute("billet", billet);
        }
        request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/billetContent.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String uri = request.getRequestURI();
        String[] splitUri = uri.split("/");
        // splitUri = ["","tp2_war","addCommentaire","groupe","id"]

        int id = Integer.parseInt(splitUri[4]);
        request.setAttribute("id", id);
        String groupe = splitUri[3];
        request.setAttribute("groupe", groupe);

        Map<String, Groupe> g = (Map<String, Groupe>) getServletContext().getAttribute("groupes");
        GestionBillets gb = g.get(groupe).getGestionBillets();
        Billet billet = gb.getBillet(id);
        request.setAttribute("billet", billet);
        request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/billetContent.jsp").forward(request, response);
    }
}
