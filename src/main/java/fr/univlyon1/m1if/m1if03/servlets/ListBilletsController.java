package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "ListBilletsController", urlPatterns = "/ListBilletsController")
public class ListBilletsController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("contenu") != null && request.getParameter("titre") != null) {
            String groupe = (String) request.getAttribute("groupe");
            Map<String, Groupe> g = (Map<String, Groupe>) request.getServletContext().getAttribute("groupes");
            fr.univlyon1.m1if.m1if03.classes.GestionBillets gb = g.get(groupe).getGestionBillets();
            Billet billet = new Billet();
            billet.setContenu(request.getParameter("contenu"));
            billet.setTitre(request.getParameter("titre"));
            billet.setAuteur((String) request.getAttribute("pseudo"));
            gb.add(billet);
            request.setAttribute("gestionBillet", gb);
            response.setStatus(201);
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/billetsGroup.jsp").forward(request, response);

        }else
            response.sendError(400,"Pas de paramètres acceptables dans la requête");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pseudo = (String) request.getAttribute("pseudo");
        String groupe = (String) request.getAttribute("groupe");

        if (groupe != null && !groupe.equals("")) {
            request.setAttribute("groupe", groupe);
            Map<String, Groupe> groupes = (Map<String, Groupe>) request.getServletContext().getAttribute("groupes");
            if (!groupes.get(groupe).getParticipants().contains(pseudo)) {
                groupes.get(groupe).addUser(pseudo);
            }
            fr.univlyon1.m1if.m1if03.classes.GestionBillets gb = groupes.get(groupe).getGestionBillets();
            request.setAttribute("gestionBillet", gb);
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/billetsGroup.jsp").forward(request, response);
        } else
            response.sendRedirect("index.html");
    }
}
