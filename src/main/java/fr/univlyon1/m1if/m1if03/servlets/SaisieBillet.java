package fr.univlyon1.m1if.m1if03.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SaisieBillet", urlPatterns = "/SaisieBillet/*")
public class SaisieBillet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String  uri = request.getRequestURI();
        String[] splitUri = uri.split("/");
        // splitUri = ["","tp2_war","saisieGroupe","xyz"]
        if(splitUri.length==4){
            request.setAttribute("groupe",splitUri[3]);
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/saisie.jsp").forward(request, response);
        } else {
            request.getServletContext().getRequestDispatcher("/groupes").forward(request, response);
        }

    }
}
