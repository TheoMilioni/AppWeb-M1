package fr.univlyon1.m1if.m1if03.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "Filter_Authentification")
public class Filter_Authentification implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        /* Non-filtrage de l'index */
        String fichier = request.getRequestURI();
        String[] uri = fichier.split("/");
        if (uri.length > 2) {
            if (uri[2].equals("index.html") || uri[2].equals("users") || uri[2].equals("client")) {
                chain.doFilter(request, response);
                return;
            }
        }
        String token = getCookieValue(request, "token");
        if (token != null) {
            try {
                Algorithm algorithm = Algorithm.HMAC256("secret");
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("mescopains")
                        .acceptLeeway(1)
                        .build(); //Reusable verifier instance
                DecodedJWT jwt = verifier.verify(token);
                request.setAttribute("pseudo", jwt.getSubject());
                chain.doFilter(req, resp);
            } catch (JWTVerificationException exception) {
                //Invalid signature/claims
                response.setStatus(401);
                request.getServletContext().getRequestDispatcher("/Init").forward(request, response);
            }
        } else {
            response.setStatus(401);
            request.getServletContext().getRequestDispatcher("/Init").forward(request, response);
        }

    }

    private static String getCookieValue(HttpServletRequest request, String nom) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie != null && nom.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }


    public void init(FilterConfig config) throws ServletException {

    }

}
