package fr.univlyon1.m1if.m1if03.filters;

import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebFilter(filterName = "Filter_Autorisation")
public class Filter_Autorisation implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String groupe = (String) request.getAttribute("groupe");
        String pseudo = (String) request.getAttribute("pseudo");
        Map<String, Groupe> groupes = (Map<String, Groupe>) request.getServletContext().getAttribute("groupes");

        if (!groupes.get(groupe).getParticipants().contains(pseudo)) {
            request.getServletContext().getRequestDispatcher("/groupes").forward(request, response);
        } else {
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
