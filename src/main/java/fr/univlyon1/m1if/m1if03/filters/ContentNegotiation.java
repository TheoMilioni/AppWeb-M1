package fr.univlyon1.m1if.m1if03.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "ContentNegotiation")
public class ContentNegotiation implements Filter {
    FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        chain.doFilter(req, resp);
        String view = (String) ((HttpServletRequest) req).getAttribute("view");
        if(((HttpServletRequest) req).getHeader("Accept").startsWith("application/json")) {
            System.out.println("un json");
        }
        // S'il y a un contenu Ã  renvoyer
        if (view != null) {
            // TODO implÃ©menter ici la nÃ©gociation de contenus

            if(((HttpServletRequest) req).getHeader("Accept").startsWith("text/html")) {
                // Cas des JSP (nommÃ©es dans le web.xml)
                RequestDispatcher dispatcher = filterConfig.getServletContext().getNamedDispatcher(view + "-vue");
                HttpServletRequest wrapped = new HttpServletRequestWrapper((HttpServletRequest) req) {
                    public String getServletPath() {
                        return "";
                    }
                };
                dispatcher.forward(wrapped, resp);
            }
        }
    }
}