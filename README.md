# M1IF03 2019
## Préambule
Souleymane BARRY : 11512253  
Théo MILIONI : 11408790
## Section TP2 & TP3
### Déploiement 
https://192.168.75.88/api/v1/
### Description
##### Aspect général de l'application

Sur chaque page est inclue un menu pour permettre d'afficher la liste des groupes ou se déconnecter à n'importe quel moment.  
Sur la page des groupes, il est possible de rejoindre un groupe déjà existant ou en créer un.  
Lorsque l'on rejoint un groupe, La liste des billets de ce groupe est disponible.  
On peut alors créer des billets ou consulter des billets déja existants pour voir les commentaires.
##### Aspect technique de l'application
Lors de l'affichage des groupes et l'affichage des billets d'un groupe, la page est actualisée toutes les 5 secondes.  
Pour l'affichage des commentaires sur un billet sélectionné, 
les commentaires  sont affichés grâce à un \<iframe\> actualisé toutes les 5 secondes.  
Nous n'avons pas réussi à implémenter la gestion de cache par les cookies ou par les headers HTTP car les headers `Last-Modified` ou `If-Modified-Since` n'apparaissaient dans les requêtes et nous n'avons pas trouvé de solutions pour les faire apparaître.  
Nous avons utilisé en attribut du scope *Session* le groupe et le pseudo de l'utilisateur, en attribut du scope *Application* l'objet Map<String, Groupe>. Un Groupe à un objet GestionBillet associé.

## Section TP4
### Déploiement
l'adresse  suivante est fonctionelle : http://192.168.75.88:8080/v2  
Nous avons eu des problèmes d'url non résolus c'est pourquoi l'adresse https://192.168.75.88/api/v2 n'est pas fonctionnelle.  

### Description
Notre API renvoie uniquement les données au **format HTML** à part pour la liste des utilisateurs qui est renvoyée en JSON.  
La vue de l'application (à partir du navigateur) est fonctionnelle et les URI type REST sont correctes.  
Nous avons eu quelques problèmes avec l'authentification JWT c'est pourquoi l'authentification ne fonctionne pas sur Swagger (problème de renvoie du header Authorization).  
Cependant avec Postman et sa gestion des cookies nous pouvons nous authentifier et utiliser l'API.   

   ***authentification (Postman):***    
```
curl --location --request POST 'http://192.168.75.88:8080/v2/users/login' \
--header 'Content-Type: application/json' \
--data-raw '{`
	"pseudo": "monPseudo"
}'
```
Chaque réponse renvoie une vue html de l'application avec un code d'erreur. 

Les requêtes **PUT** et **DELETE** n'ont pas été codés.  
La représentation d'un groupe : *GET /groupes/{groupeId}* ne fonctionne pas.  
Les commentaires sont fonctionnels sur l'application (navigateur), mais ne sont pas pris en charge par l'API.  

Tous les autres appels sont fonctionnels.  

Pour toutes les requêtes POST, l'API ne prend en charge que des paramètres de requêtes de type *x-www-form-urlencoded* et non pas un body de type JSON comme spécifié sur le .yaml.  
Pour l'ajout d'un groupe : ***POST /groupes***, cela se fait avec la requête suivante (le paramètre n'est pas seulement un string mais *groupe=nomGroupe* :   
```
curl --location --request POST 'http://192.168.75.88:8080/v2/groupes' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Accept: application/json' \
--data-urlencode 'groupe=nomGroupe'
```
Pour l'ajout d'un billet : ***POST /groupes/nomGroupe/billets***, cela se fait de la même manière que précédemment :
```
curl --location --request POST 'http://192.168.75.88:8080/v2/groupes/monGroupe/billets' \
--header 'Authorization: ' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'contenu=monContenu' \
--data-urlencode 'titre=monTitre'
```

## Section TP5 & TP7
### Déploiement
Notre SPA est déployé sur https://192.168.75.88/api/v2/client.  
Nous n'avons pas réussi à le mettre à la racine du serveur.  
### Description

#### TP5
L'API requêté par notre SPA est l'API de correction : *192.168.75.13*.  
Nous n'avons pas utilisé les requêtes PUT et DELETE. Nous ne pouvons donc pas modifier ou supprimer des éléments.   
Les commentaires ne se rechargent pas automatiquement mais sont fonctionnels.
#### TP7
##### Mesures des temps (déploiement sur Tomcat à partir d'un VPN)
Nous avons réutilisé le script du TP6 pour effectuer les mesures suivantes :  
* Temps de chargement HTML : **4272 ms**
* Temps d'affichage App shell : **271 ms**
* Temps CRP : **553 ms**

Le reste du TP7 n'a pas été réalisé.







 
 

