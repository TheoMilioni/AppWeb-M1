//mecanisme de routage qui remplace la classe (stylée) active ou inactive
var groupe = "";
var pseudo = "";

function show(hash) {
    $('.active').removeClass('active').addClass('inactive');
    $(hash).removeClass('inactive').addClass('active');
    hashrequest = hash.substr(1); // On supprime le #
    if (hashrequest.localeCompare("groupes") === 0 || hashrequest.localeCompare("users") === 0) {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: 'https://192.168.75.13/api/v2/' + hashrequest,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            success: function (response) {
                response.forEach(function(el, index, theArray){
                    theArray[index]= getName(el);
                })
                loadData(hashrequest, response);
            },
            error: function (resultat, statut, erreur) {
                console.log(statut)
            },
        });
    } else if (hashrequest.localeCompare("groupe") === 0) {
        groupe = localStorage.getItem("groupe");
        $.ajax({
            type: "GET",
            dataType: "json",
            url: 'https://192.168.75.13/api/v2/groupes/' + groupe,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            success: function (response) {
                response.auteur = getName(response.auteur);
                response.membres.forEach(function(el, index, theArray){
                    theArray[index]= getName(el);
                })
                //permet d'afficher le titre d'un billet
                response.billets.forEach(function(el, index, theArray) {
                    theArray[index] = getElement(el).titre;
                });
                loadData(hashrequest, response);
            },
            error: function (resultat, statut, erreur) {
                console.log(statut)
            }
        });

    } else if (hashrequest.localeCompare("billet") === 0) {
        groupe = localStorage.getItem("groupe");
        lastBilletID = localStorage.getItem("lastBilletID") - 1;
        $.ajax({
            type: "GET",
            dataType: "json",
            url: 'https://192.168.75.13/api/v2/groupes/' + groupe + '/billets/' + lastBilletID,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            success: function (response) {
                response.auteur = getName(response.auteur);
                response.commentaires.forEach(function(el, index, theArray){
                    theArray[index] = getElement(el);
                    theArray[index].auteur = getName(theArray[index].auteur);

                });
                loadData(hashrequest, response);
            },
            error: function (resultat, statut, erreur) {
                console.log(statut);
                $("#errMsg").append(statut + ": Impossible d'acceder au billet numéro " + lastBilletID);

            },
        });

    }
}

//Transforme https://192.168.75.13/api/v2/groupes/nomGroupe => nomGroupe
function getName(url) {
    let n = url.split("/");
    return n[n.length - 1];
}

// permet de recupérer le JSON d'un billet ou d'un commentaire à partir de son URL
function getElement(url) {
    return $.ajax({
        type: "GET",
        dataType: "json",
        url: url,
        global: false,
        async: false,
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        success: function(response) {
            return response;
        }
    }).responseJSON;
}
// Affiche et cache à chaque clic le menu
$(function () {
    $('#menu').click(function () {
        $('#listmenu').toggle();
    });
});

//Permet de changer de vue
window.addEventListener('hashchange', function (event) {
    show(window.location.hash);
});


// Le rendu avec Mustache
function loadData(hash, response) {
    // récupération du template
    var template = $('#tpl' + hash).html();

    // optionnel, accélère les utilisations futures
    Mustache.parse(template);

    // génération du HTML
    var rendered = Mustache.render(template, response);

    // Insertion du résultat dans la page HTML
    $('#target' + hash).html(rendered);
}

$('#connexion').click(function (e) {
    e.preventDefault();
    if ($('#pseudo').val() != "") {
        pseudo = encodeURIComponent($('#pseudo').val()); // on sécurise les données
    }
    requestData = JSON.stringify({"pseudo": pseudo});

    $.ajax({
        url: "https://192.168.75.13/api/v2/users/login",
        type: 'POST',
        xhrFields: {withCredentials: true},
        crossDomain: true,
        data: requestData,
        datatype: "json",
        headers: {
            "Content-Type": "application/json",
        }
    }).done(function (data, textstatus, xhr) {
        localStorage.setItem("pseudo", pseudo);
        localStorage.setItem("token", xhr.getResponseHeader("Authorization"));
        window.location = "#groupes";
    }).fail(function () {
        $("#errMsg").append("echec vers: https://192.168.75.13/api/v2/users/login");
    });
});
$('#nouveauGroupe').click(function (e) {
    e.preventDefault();
    groupe = $("#nomGroupe").val();
    requestData = JSON.stringify({"nom": groupe});
    $.ajax({
        url: "https://192.168.75.13/api/v2/groupes",
        type: 'POST',
        xhrFields: {withCredentials: true},
        crossDomain: true,
        data: requestData,
        datatype: "json",
        headers: {
            "Content-Type": "application/json",
        }
    }).done(function () {
        console.log("Le groupe " + groupe + " a été ajouté avec succès.")
        localStorage.setItem("groupe", groupe);
        window.location = "#groupe";
    }).fail(function () {
        $("#errMsg").append("Impossible d'ajouter le groupe");
    });
});

$('#addBillet').click(function (e) {
    e.preventDefault();
    groupe = localStorage.getItem("groupe");
    pseudo = localStorage.getItem("pseudo");
    titre = $("#titre").val();
    description = $("#contenu").val();
    requestData = JSON.stringify({
        "titre": titre,
        "contenu": description,
        "auteur": pseudo,
        "commentaires": []
    });
    $.ajax({
        url: 'https://192.168.75.13/api/v2/groupes/' + groupe + '/billets',
        type: 'POST',
        crossDomain: true,
        data: requestData,
        datatype: 'json',
        xhrFields: {withCredentials: true},
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('token')
        }
    }).done(function () {
        console.log("Le billet : " + titre + " a été ajouté avec succès");
        /*On recupère ensuite la liste des billets pour avoir
         * le numéro du dernier billet ajouté */
        $.ajax({
            type: "GET",
            dataType: "json",
            url: 'https://192.168.75.13/api/v2/groupes/' + groupe + '/billets',
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            success: function (response) {
                localStorage.setItem("lastBilletID", response.length);
                window.location = "#billet";
            },
            error: function (resultat, statut, erreur) {
                $("#errMsg").append("Impossible de stocker le nombre de billets");
            },
        });
    }).fail(function (xhr, textStatus, errorThrown) {
        $("#errMsg").append("Le billet " + titre + " n'a pas été ajouté : ");
    });
});

$('#addComment').click(function (e) {
    e.preventDefault();
    groupe = localStorage.getItem("groupe");
    pseudo = localStorage.getItem("pseudo");
    idBillet = localStorage.getItem("lastBilletID")-1;
    commentaire = $("#commentaire").val();
    requestData = JSON.stringify({
        "auteur": pseudo,
        "texte": commentaire
    });
    $.ajax({
        url: 'https://192.168.75.13/api/v2/groupes/'+groupe+'/billets/'+idBillet+'/commentaires',
        type: 'POST',
        crossDomain: true,
        data: requestData,
        datatype: 'json',
        xhrFields: {withCredentials: true},
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('token')
        }
    }).done(function () {
        console.log("Le commentaire : " + commentaire + " a été ajouté avec succès");
    }).fail(function (xhr, textStatus, errorThrown) {
        $("#errMsg").append("Le commentaire " + commentaire + " n'a pas été ajouté : ");
    });
});

$('#Deconnexion').click(function (e) {
    e.preventDefault();
    pseudo = localStorage.getItem("pseudo");
    $.ajax({
        url: 'https://192.168.75.13/api/v2/users/logout',
        type: 'POST',
        crossDomain: true,
        datatype: 'json',
        xhrFields: {withCredentials: true},
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('token')
        }
    }).done(function () {
        localStorage.setItem('token',"");
        pseudo="";
        localStorage.setItem("pseudo",pseudo);
        console.log("Utilisateur: " + pseudo + " a été déconnecté");
        alert("Utilisateur déconnecté");
        window.location="#index";
    }).fail(function (xhr, textStatus, errorThrown) {
        $("#errMsg").append("Impossible de déconnecter l'utlisateur" + pseudo);
    });
});